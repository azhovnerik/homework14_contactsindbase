package utils;

import services.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Menu {
    MenuItem[] items ;
    List<MenuItem> itemsVisible = new ArrayList<>(); //меню только с видимыми колонками
    Scanner scanner ;

    public Menu(Scanner scanner, MenuItem[] items) {
        this.items = items;
        this.scanner = scanner;

    }
    public void run(){
        while(true){
            showMenu();
            int choice = getUserChoice();
            if(choice<0||choice>=itemsVisible.size()){
                System.out.println("Incorrect choice");
                continue;

            }
            itemsVisible.get(choice).execute();
            if(itemsVisible.get(choice).isFinal()) break;
        }
    }

    private int getUserChoice() {
        System.out.println("Enter your choice");
        int ch = scanner.nextInt();
        scanner.nextLine();
        return ch-1;
    }

    private void showMenu() {
        System.out.println("---------------------------------");
        itemsVisible.clear();
        for (int i = 0; i < items.length; i++) {
           if (items[i].isVisible()) {
               itemsVisible.add(items[i]);
               //System.out.printf("%2d - %s\n", i + 1, items[i].getName());
           }
        }
        //выведеме тольео видимые пункты с  нумерацией по порядку
        for (int i = 0; i < itemsVisible.size(); i++) {

                System.out.printf("%2d - %s\n", i + 1, itemsVisible.get(i).getName());

        }

        System.out.println("---------------------------------");
    }
}


