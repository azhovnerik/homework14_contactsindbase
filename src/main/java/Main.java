import com.fasterxml.jackson.databind.ObjectMapper;
import entities.Contact;
import persistence.*;
import services.*;
import ui.controllers.*;
import ui.views.ContactView;
import ui.views.DefaultContactView;
import utils.Menu;
import utils.validator.Validator;
import utils.validator.contact.ContactValidator;
import utils.validator.contact.EmailValidator;
import utils.validator.contact.PhoneValidator;

import java.io.IOException;
import java.util.*;

public class Main {

    private static final String APPLICATION_MODE_KEY = "application.mode";

    public static void main(String[] args) {
        ContactRepositoryFactory repositoryFactory = getContactRepositoryFactory();
        Scanner scanner = new Scanner(System.in);

        Validator<Contact> contactValidator = new ContactValidator(List.of(
                new EmailValidator(),
                new PhoneValidator()));

        // ContactRepository contactRepository = getContactRepository();
        ContactRepository contactRepository = repositoryFactory.createContactRepositort();
        ContactService contactService = new ContactService(contactRepository, contactValidator);

        ContactView contactView = new DefaultContactView(scanner);
      //  NetworkContactRepository  networkContactRepository = (NetworkContactRepository) contactRepository; //сделал каст потому что не знаю как по-другому получить поле as из ContactRepository
        Menu menu = new Menu(scanner, new MenuItem[]{new AddContactMenuItem(contactService, scanner, contactView),
                new SelectContactsWithSorting(contactService, contactView),
                new SelectOnlyPhones(contactService, contactView),
                new SelectOnlyEmails(contactService, contactView),
                new SearchByName(contactService, contactView, scanner),
                new SearchByStartOfContact(contactService, contactView, scanner),
                new DeleteContactByValue(contactService, scanner),
                new NetworkGetUsers(contactService, contactView),
               //new NetworkRegisterNewUser(contactService, contactView,networkContactRepository.getAuthService()),
                new NetworkRegisterNewUser(contactService, contactView),
                new NetworkLogin(contactService, contactView),
                new ExitMenuItem()});
        menu.run();
    }

    private static ContactRepositoryFactory getContactRepositoryFactory() {
        Properties properties = new Properties();
        String fileName = "";
        try {
            properties.load(Main.class.getClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            properties.setProperty(APPLICATION_MODE_KEY, "IN_MEMORY");
        }
        String mode = properties.getProperty(APPLICATION_MODE_KEY);
        if (mode != "IN_MEMORY") {
            fileName = properties.getProperty("FILE_NAME");
        }

        Map<String, ContactRepositoryFactory> factories = Map.of(
                "IN_MEMORY", new InMemoryContactRepositoryFactory(),
                "JSON_FILE", new JsonFileContactRepositoryFactory(properties),
                "NETWORK", new NetworkContactRepositoryFactory(properties),
                "DBASE", new DbaseContactRepositoryFactory(properties)
        );
        if (!factories.containsKey(mode)) {
            throw new RuntimeException("INVALID MODE CONFIGURATION");
        }
        return factories.get(mode);
    }

/*    private static ContactRepository getContactRepository() {
        //System.getProperties();
        ObjectMapper objectMapper = new ObjectMapper();
        Properties properties = new Properties();
        String fileName = "";
        try {
            properties.load(Main.class.getClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            properties.setProperty(APPLICATION_MODE_KEY, "IN_MEMORY");   //"application.mode"
        }
        String mode = properties.getProperty(APPLICATION_MODE_KEY);
        if (mode != "IN_MEMORY") {
            fileName = properties.getProperty("FILE_NAME");
        }

        Map<String, ContactRepository> contactRepositoryMap = Map.of(
                "IN_MEMORY", new InMemoryContactRepository(),
                "JSON_FILE", new JsonFileContactRepository(objectMapper, fileName),
                "TXT_FILE", new TxtFileContactRepository(fileName),
                "BYTE_FILE", new ByteSerializeFileContactRepository(fileName),
                "NETWORK", new NetworkContactRepository());

        if (!contactRepositoryMap.containsKey(mode)){
            throw new RuntimeException("INVALID MODE CONFIGURATION");
        }

        return contactRepositoryMap.get(mode);
    }*/

}



