import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import persistence.AbstractFileContactsRepository;
import persistence.CachedContactRepository;
import persistence.ContactRepository;
import persistence.JsonFileContactRepository;
import services.AuthService;
import services.InMemoryAuthService;

import java.util.Properties;
@RequiredArgsConstructor
public class JsonFileContactRepositoryFactory implements ContactRepositoryFactory {
    private final Properties properties;
    @Override
    public ContactRepository createContactRepositort() {
        return new CachedContactRepository(new JsonFileContactRepository(createObjectMapper(),properties.getProperty("FILE_NAME")));


    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }

    private ObjectMapper createObjectMapper(){
        return new ObjectMapper();
    }
}
