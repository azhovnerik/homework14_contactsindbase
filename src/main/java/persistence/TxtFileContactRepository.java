package persistence;

import entities.Contact;
import entities.TypeContact;
import entities.User;
import lombok.RequiredArgsConstructor;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class TxtFileContactRepository extends AbstractFileContactsRepository {
    private final String fileName;
    @Override
    protected List<Contact> readAll() {
        List<Contact> contactList = new ArrayList<>();
              //read contactList from file
        try (InputStream is = new FileInputStream(fileName)) {
            Scanner scanner = new Scanner(is);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.trim().isEmpty()) continue;
                String[] parts = line.split(":");
                String surname = parts[0];
                String name = parts[1];
                String patronomic = parts[2];
                TypeContact type = Objects.equals(parts[3], "EMAIL") ? TypeContact.EMAIL : TypeContact.PHONE;
                String value = parts[4];
                String uuid = parts[5];
                contactList.add(new Contact(surname, name, patronomic, type, value, uuid));

            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
        return contactList;
    }

    @Override
    protected void saveAll(List<Contact> contacts) {
             String contactListString = contacts.stream().map(cnt -> cnt.toString()).collect(Collectors.joining("\r\n"));
            try (OutputStream os = new FileOutputStream(fileName)) {
                os.write(contactListString.getBytes(StandardCharsets.UTF_8));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    @Override
    public List<User> getAllUsers() {
        throw new UnsupportedOperationException();
    }
    }

