package persistence;

import db.BeanPropertyRowMapper;
import db.JdbcTemplate;
import db.Md5PasswordEncoder;
import dto.ContactDbDto;
import entities.Contact;
import entities.TypeContact;
import entities.User;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import services.DbaseAuthService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@RequiredArgsConstructor
@Getter
public class DbaseContactRepository implements ContactRepository {
    private final DbaseAuthService authService ;
    private final JdbcTemplate jdbcTemplate;


   // public static final String HOST = "https://mag-contacts-api.herokuapp.com";

    @Override
    public void addContact(Contact contact) {
       /* insert into contacts ( name, type_id, value) values
                ('boris','1','a@gmail.com');*/
       jdbcTemplate.execute("insert into contacts ( name, type_id, value) values\n" +
               "('"+contact.getName()+"','"+getTypeId(contact.getType())+"','"+contact.getValue()+"')");


    }

    private  int getTypeId(TypeContact typeContact){
        if (typeContact==TypeContact.EMAIL){
           return 1;
        } else if (typeContact==TypeContact.PHONE){
            return 2;
        }
        return 0;
    }

    @Override
    public void deleteContact(String value) {
       // delete from contacts where value='0675333333'
        jdbcTemplate.execute("delete from contacts where value='"+value+"'");
    }

    @Override
    public List<Contact> searchByName(String namePart) {
        List<ContactDbDto> contactDbDtoList =   jdbcTemplate.query("select id, name, type_id, value from contacts where name LIKE '%"+namePart+"%' ",new BeanPropertyRowMapper<>(ContactDbDto.class));
        return  convertToContactList(contactDbDtoList);
    }

    private List<Contact> convertToContactList(List<ContactDbDto> contactDbDtoList) {
        List<Contact>  contactList= new ArrayList<>();
        for (ContactDbDto contactDbDto : contactDbDtoList
             ) {
            Contact contact = new Contact("",contactDbDto.getName(),"",TypeContact.getTypeFromTypeId(contactDbDto.getType_Id()),contactDbDto.getValue());
            contactList.add(contact);

        }
        return contactList;
    }
    private Contact convertToContact(ContactDbDto contactDbDto) {
            Contact contact = new Contact("",contactDbDto.getName(),"",TypeContact.getTypeFromTypeId(contactDbDto.getType_Id()),contactDbDto.getValue());
            return contact;

    }


    @Override
    public List<Contact> SearchByStartOfContact(String valueStart) {
        List<ContactDbDto> contactDbDtoList =   jdbcTemplate.query("select id, name, type_id, value from contacts where value LIKE '%"+valueStart+"%' ",new BeanPropertyRowMapper<>(ContactDbDto.class));
        return  convertToContactList(contactDbDtoList);
    }

    @Override
    public List<Contact> SelectContactsWithSorting() {
        List<ContactDbDto> contactDbDtoList =   jdbcTemplate.query("select id, name, type_id, value from contacts",new BeanPropertyRowMapper<>(ContactDbDto.class));
        return  convertToContactList(contactDbDtoList);
    }

    @Override
    public List<Contact> SelectByType(TypeContact type) {

        List<ContactDbDto> contactDbDtoList =   jdbcTemplate.query("select id, name, type_id, value from contacts where type_id="+getTypeId(type),new BeanPropertyRowMapper<>(ContactDbDto.class));
        return  convertToContactList(contactDbDtoList);
    }

    @Override
    public Optional<Contact> SearchByValue(String value) {
        List<ContactDbDto> contactDbDtoList =   jdbcTemplate.query("select id, name, type_id, value from contacts where value = '"+value+"'",new BeanPropertyRowMapper<>(ContactDbDto.class));
        if (contactDbDtoList.size()==0) {
            return Optional.empty();
        }
        else
            {
            ContactDbDto contactDbDto = contactDbDtoList.get(0);
            Contact contact = convertToContact(contactDbDto);
            //return Optional.ofNullable(contact).orElse(contact);
            return Optional.of(contact);

            }

    }




    @Override
    public List<User> getAllUsers() {
        return jdbcTemplate.query("select login, date_born, password from users",new BeanPropertyRowMapper<>(User.class));
    }
}
