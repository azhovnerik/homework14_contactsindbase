package persistence;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import entities.Contact;
import entities.User;
import lombok.RequiredArgsConstructor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
@RequiredArgsConstructor
public class JsonFileContactRepository extends AbstractFileContactsRepository {
    private final ObjectMapper objectMapper;
    private final String fileName;
    @Override
    protected List<Contact> readAll() {
        try(InputStream fis = new FileInputStream(fileName)){
            return objectMapper.readValue(fis, new TypeReference<ArrayList<Contact>>(){}) ;
            } catch (IOException e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    protected void saveAll(List<Contact> contacts) {
      try(OutputStream os = new FileOutputStream(fileName)){
          byte[] jsonBytes =  objectMapper.writeValueAsBytes(contacts);
          os.write(jsonBytes);
          os.flush();
      } catch (IOException e){
          e.printStackTrace();

      }
    }

    @Override
    public List<User> getAllUsers() {
        throw new UnsupportedOperationException();
    }
}
