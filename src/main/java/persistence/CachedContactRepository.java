package persistence;

import entities.Contact;
import entities.TypeContact;
import entities.User;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
@RequiredArgsConstructor
public class CachedContactRepository implements ContactRepository {
    private final ContactRepository repository;
    private  List<Contact> cacheAll;
    private Map<TypeContact,List<Contact>> cacheType = new HashMap<>();
    private void invalidateCache(){
        cacheAll = null;
        cacheType.clear();
    }


    @Override
    public void addContact(Contact contact) {
      repository.addContact(contact);
      invalidateCache();
    }

    @Override
    public void deleteContact(String value) {
     repository.deleteContact(value);
     invalidateCache();
    }

    @Override
    public List<Contact> searchByName(String namePart) {
        return repository.searchByName(namePart);
    }

    @Override
    public List<Contact> SearchByStartOfContact(String valueStart) {
        return repository.SearchByStartOfContact(valueStart);
    }

    @Override
    public List<Contact> SelectContactsWithSorting() {
        if(cacheAll==null){
            cacheAll = repository.SelectContactsWithSorting();
        }
        return cacheAll;
    }

    @Override
    public List<Contact> SelectByType(TypeContact type) {
        if (!cacheType.containsKey(type)){
            cacheType.put(type,repository.SelectByType(type));
        }
        return cacheType.get(type);
    }

    @Override
    public Optional<Contact> SearchByValue(String value) {
        return repository.SearchByValue(value);
    }
    @Override
    public List<User> getAllUsers() {
        return  repository.getAllUsers();
    }
}
