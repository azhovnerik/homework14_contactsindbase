package persistence;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import entities.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import services.AuthService;
import services.NetworkAuthService;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

@RequiredArgsConstructor
@Getter
public class NetworkContactRepository implements ContactRepository{
    private final  NetworkAuthService authService ;
    public static final String HOST = "https://mag-contacts-api.herokuapp.com";



    @Override
    public void addContact(Contact contact) {
        String token = authService.getToken();
        //создадим мапу с параметрами пользователя
        Map<String,String> map = new HashMap<>() ;
        map.put("type",contact.getType().toString());
        map.put("value",contact.getValue());
        map.put("name",contact.getName());
        String requestBody = getRequestBody(map);

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(HOST+"/contacts/add"))
                .header("Authorization","Bearer "+token)
                .header("Accept","application/json")
                .header("Content-Type","application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();
        try {

            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            Map<String,String> result = new ObjectMapper().readValue(response.body(),HashMap.class);
            if (result.get("status").equals("ok")) {
                System.out.println("Добавлен контакт "+contact.toString()+" в сетевой репозиторий!");
            } else
            {
                System.out.println("Ошибка добавления контакта "+contact.toString());
            }

            //System.out.println(response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteContact(String value) {

    }

    @Override
    public List<Contact> searchByName(String namePart) {
        String token = authService.getToken();
        //создадим мапу с параметрами пользователя
        Map<String,String> map = new HashMap<>() ;
        map.put("name",namePart);
        String requestBody = getRequestBody(map);

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(HOST+"/contacts/find"))
                .header("Authorization","Bearer "+token)
                .header("Accept","application/json")
                .header("Content-Type","application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();
        try {

            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            ResponceGetContacts result = new ObjectMapper().readValue(response.body(),ResponceGetContacts.class);
            if (result.getStatus().equals("ok")) {
                System.out.println("Список найденных контактов сетевого репозитория:");
              /*  for (Contact contact:result.getContacts()
                ) {
                    System.out.println(contact.toString());
                }*/
                return result.convertToContacts();
            } else
            {
                System.out.println("Ошибка поиска  контакта по имени! ");
            }

            //System.out.println(response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
       return new ArrayList<>();
    }

    @Override
    public List<Contact> SearchByStartOfContact(String valueStart) {
        String token = authService.getToken();
        //создадим мапу с параметрами пользователя
        Map<String,String> map = new HashMap<>() ;
        map.put("value",valueStart);
        String requestBody = getRequestBody(map);

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(HOST+"/contacts/find"))
                .header("Authorization","Bearer "+token)
                .header("Accept","application/json")
                .header("Content-Type","application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();
        try {

            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            ResponceGetContacts result = new ObjectMapper().readValue(response.body(),ResponceGetContacts.class);
            if (result.getStatus().equals("ok")) {
                System.out.println("Список найденных по значению контактов сетевого репозитория:");
              /*  for (Contact contact:result.getContacts()
                ) {
                    System.out.println(contact.toString());
                }*/
                return result.convertToContacts();
            } else
            {
                System.out.println("Ошибка поиска  контакта по значению! ");
            }

            //System.out.println(response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public List<Contact> SelectContactsWithSorting() {
        String token = authService.getToken();
        ArrayList<Contact> lstUsers = new ArrayList<>();
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(NetworkContactRepository.HOST+"/contacts"))
                .header("Authorization","Bearer "+token)
                .header("Accept","application/json")
                .header("Content-Type","application/json")
                .GET()
                .build();
        try {

            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            ResponceGetContacts result = new ObjectMapper().readValue(response.body(), ResponceGetContacts.class);
            if (result.getStatus().equals("ok")) {
                System.out.println("Список контактов сетевого репозитория:");
                List<Contact> contactList = result.convertToContacts();
                for (Contact contact:contactList
                ) {
                    System.out.println(contact.toString());
                }
                return lstUsers;

            } else
            {
                System.out.println("Ошибка получения списка пользователей");
            }

            //System.out.println(response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return lstUsers;
    }

    @Override
    public List<Contact> SelectByType(TypeContact type) {
        return null;
    }

    @Override
    public Optional<Contact> SearchByValue(String value) {
        return Optional.empty();
    }

    String getRequestBody(Map map){

        //сконвертим мапу в json
        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody="";
        try {
            requestBody = objectMapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return requestBody;
    }
    //получить список всех пользователей сетевого репозитория

    @Override
    public List<User> getAllUsers() {
      //  authService.login("!azh","321");
        String token = authService.getToken();
        ArrayList<User> lstUsers = new ArrayList<>();
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(NetworkContactRepository.HOST+"/users"))
           //     .header("Authorization","Bearer "+token)
                .GET()
                .build();
        try {

            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            ResponceGetUsers result = new ObjectMapper().readValue(response.body(),ResponceGetUsers.class);
            if (result.getStatus().equals("ok")) {
                System.out.println("Список юзеров сетевого репозитория:");
                for (User user:result.getUsers()
                ) {
                    System.out.println(user.toString());
                }
                return lstUsers;

            } else
            {
                System.out.println("Ошибка получения списка пользователей");
            }

            //System.out.println(response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return lstUsers;
    }

}
