package ui.controllers;

import lombok.RequiredArgsConstructor;
import persistence.ContactRepository;
import persistence.DbaseContactRepository;
import persistence.NetworkContactRepository;
import services.ContactService;
import services.MenuItem;
import services.NetworkAuthService;
import ui.views.ContactView;
@RequiredArgsConstructor
public class NetworkGetUsers implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;
    @Override
    public String getName() {
        return "Repository - get users";
    }

    @Override
    public void execute() {
        contactView.showUsers(contactService.SelectAllUsers());


    }

    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository||contactRepository instanceof DbaseContactRepository){

                return true;

        }
        return false;
    }
}
