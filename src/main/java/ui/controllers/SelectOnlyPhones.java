package ui.controllers;

import lombok.RequiredArgsConstructor;
import persistence.ContactRepository;
import persistence.DbaseContactRepository;
import persistence.NetworkContactRepository;
import services.AuthService;
import services.ContactService;
import services.MenuItem;
import ui.views.ContactView;

@RequiredArgsConstructor
public class SelectOnlyPhones implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Select only phones";
    }

    @Override
    public void execute() {
      contactView.showContacts(contactService.SelectByPhone());
    }

    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository){
            return false;

        }

        if (contactRepository instanceof DbaseContactRepository){
            AuthService as = (((DbaseContactRepository) contactRepository).getAuthService());
            if (as.isAuth()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}
