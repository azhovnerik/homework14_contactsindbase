package ui.controllers;

import entities.Contact;
import exception.ContactNotFoundException;
import persistence.ContactRepository;
import persistence.DbaseContactRepository;
import persistence.NetworkContactRepository;
import services.AuthService;
import services.ContactService;
import services.MenuItem;
import services.NetworkAuthService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class DeleteContactByValue implements MenuItem {
    private Scanner scanner;
    private ContactService contactService;


    public DeleteContactByValue(ContactService contactService, Scanner scanner) {
        this.contactService = contactService;
        this.scanner = scanner;
    }

    @Override
    public String getName() {
        return "Delete contact by value";
    }

    @Override
    public void execute() {
        System.out.print(" Введите значение контакта который нужно удалить:");
        String value = scanner.nextLine();
        try {
            contactService.deleteContact(value);
        } catch (ContactNotFoundException ex) {
            System.out.println("Контакт не найден!");
        }
    }

    @Override
    public boolean isVisible() {
        ContactRepository contactRepository = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository) {

            return false;

        } else if (contactRepository instanceof DbaseContactRepository) {
            AuthService as = (((DbaseContactRepository) contactRepository).getAuthService());
            if (as.isAuth()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}
