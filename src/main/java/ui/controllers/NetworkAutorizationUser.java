package ui.controllers;

import exception.ContactNotFoundException;
import lombok.RequiredArgsConstructor;
import persistence.ContactRepository;
import persistence.DbaseContactRepository;
import persistence.NetworkContactRepository;
import services.AuthService;
import services.ContactService;
import services.MenuItem;
import services.NetworkAuthService;

import java.util.Scanner;
@RequiredArgsConstructor
public class NetworkAutorizationUser implements MenuItem {
    private final Scanner scanner;
    private final ContactService contactService;
    @Override
    public String getName() {
        return "Network repository autorization";
    }

    @Override
    public void execute() {
        System.out.print(" Введите логин:");
        String login = scanner.nextLine();
        System.out.print(" Введите пароль:");
        String pass = scanner.nextLine();
        try {
            NetworkContactRepository networkContactRepository = (NetworkContactRepository) contactService.getContactRepository();
            networkContactRepository.getAuthService().login(login,pass);

        }catch (ContactNotFoundException ex) {
            System.out.println("Контакт не найден!");
        }
    }

    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository || contactRepository instanceof DbaseContactRepository){

                return true;

        }
        return false;
    }
}
