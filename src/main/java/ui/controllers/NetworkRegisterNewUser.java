package ui.controllers;

import db.JdbcTemplate;
import entities.User;

import lombok.RequiredArgsConstructor;
import persistence.ContactRepository;
import persistence.DbaseContactRepository;
import persistence.NetworkContactRepository;
import services.AuthService;
import services.ContactService;
import services.MenuItem;
import ui.views.ContactView;

@RequiredArgsConstructor
public class NetworkRegisterNewUser implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;
  //  private final AuthService authService;
    @Override
    public String getName() {
        return "Repository - register new  user";
    }

    @Override
    public void execute() {
        User user = contactView.readNewUser();
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository) {
            NetworkContactRepository networkContactRepository = (NetworkContactRepository) contactRepository;
            //authService.register(user.getLogin(),user.getDate_born(),user.getPassword());
            networkContactRepository.getAuthService().register(user.getLogin(), user.getDate_born(), user.getPassword());
        }  else  {
            DbaseContactRepository dbaseContactRepository = (DbaseContactRepository) contactRepository;
            JdbcTemplate  jdbcTemplate=dbaseContactRepository.getJdbcTemplate();
            //authService.register(user.getLogin(),user.getDate_born(),user.getPassword());
            dbaseContactRepository.getAuthService().register(user.getLogin(), user.getDate_born(), user.getPassword());
        }

    }

    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository || contactRepository instanceof DbaseContactRepository){

            return true;

        }
        return false;
    }
}
