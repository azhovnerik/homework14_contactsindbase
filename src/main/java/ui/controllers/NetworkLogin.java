package ui.controllers;

import entities.User;
import lombok.RequiredArgsConstructor;
import persistence.ContactRepository;
import persistence.DbaseContactRepository;
import persistence.NetworkContactRepository;
import services.AuthService;
import services.ContactService;
import services.MenuItem;
import ui.views.ContactView;

@RequiredArgsConstructor
public class NetworkLogin implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;
   // private final AuthService authService;
    @Override
    public String getName() {
        return "Repository - login";
    }

    @Override
    public void execute() {
        User user = contactView.readUserForLogin();
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository) {
          NetworkContactRepository networkContactRepository = (NetworkContactRepository) contactRepository;
          networkContactRepository.getAuthService().login(user.getLogin(),user.getPassword());
        }
          else if (contactRepository instanceof DbaseContactRepository){
            DbaseContactRepository dbaseContactRepository = (DbaseContactRepository) contactRepository;
            dbaseContactRepository.getAuthService().login(user.getLogin(),user.getPassword());
          }



    }
    @Override
    public boolean isVisible() {
        ContactRepository contactRepository  = contactService.getContactRepository();
        if (contactRepository instanceof NetworkContactRepository || contactRepository instanceof DbaseContactRepository){

            return true;

        }
        return false;
    }
}
