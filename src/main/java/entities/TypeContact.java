package entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TypeContact {


    EMAIL("email:"),

    PHONE("phone:");
    private String name;

    TypeContact(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public  static TypeContact getTypeFromTypeId(int typeId) {
        for (TypeContact type:TypeContact.values()
        ) {
            if (type.ordinal()+1==typeId) return type;
        }
        return null;
    }
}
