package entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;
import java.util.UUID;

@Data
@Setter
@RequiredArgsConstructor
public class Contact implements Serializable {
  //  @JsonProperty("id")
    private String uuid;
    private String surname;
    private String name;
    private String patronomic;
    private TypeContact type;
    private String value;
   // private String fullName;

    public Contact(String surname, String name, String patronomic, TypeContact type, String value) {
        this.surname = surname;
        this.name = name;
        this.patronomic = patronomic;
      //  this.fullName = surname+" "+name+" "+patronomic;
        this.type = type;
        this.value = value;
        this.uuid = UUID.randomUUID().toString();
    }
    public Contact(String surname, String name, String patronomic, TypeContact type, String value, String uuid) {
        this.surname = surname;
        this.name = name;
        this.patronomic = patronomic;
     //   this.fullName = surname+" "+name+" "+patronomic;
        this.type = type;
        this.value = value;
        this.uuid = uuid;
    }
   /* private String email;
    private String phone;
*/
   @JsonIgnore
    public String getFullName(){
        return surname+" "+name+" "+patronomic;
    }
    @Override
    public String toString() {
        return  surname+":"+name+":"+patronomic+":"+ type.getName()+value+":"+uuid;
    }

    public static Comparator<Contact> bySurnameComparator(){
       return (Contact a,Contact b) -> {
           if (a==null&&b==null) return 0;
           if (a==null) return -1;
           if (b==null) return 1;

            return a.surname.compareTo(b.surname);
           //Objects.compare(Contact a,Contact b,String::compareTo);
       };
    }
}
