package entities;

import dto.ContactDto;
import dto.TypeContactDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Getter
public class ResponceGetContacts {
      List<ContactDto> contacts;
      private String status;
      public List<Contact> convertToContacts(){
            List<Contact>  contactList = new ArrayList<>();
            for (ContactDto cnt: contacts){
                  contactList.add(new Contact(null,cnt.getName(),null,getType(cnt.getType()),cnt.getValue()));


            }
            return  contactList;
      }

      TypeContact  getType(TypeContactDto typeContactDto) {
            if (typeContactDto == TypeContactDto.EMAIL){
                  return TypeContact.EMAIL;
            } else if (typeContactDto == TypeContactDto.PHONE) {
                  return TypeContact.PHONE;
            }
            return null;
      }
}
