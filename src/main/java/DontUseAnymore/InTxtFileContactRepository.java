/*
package DontUseAnymore;

import entities.Contact;
import entities.TypeContact;
import persistence.ContactRepository;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;


public class InTxtFileContactRepository implements ContactRepository {
    List<Contact> contactList = new ArrayList<>();

    private OutputStream os;  // =

    public InTxtFileContactRepository() {
        readContactListFromFile(); //read existing contacts from file when object is created
    }

    @Override
    public void addContact(Contact contact) {
        //readContactListFromFile();
        //add new contact to contactList
        contact.setUuid(UUID.randomUUID().toString());
        contactList.add(contact);
        writeContactListToFile();
    }

    private void writeContactListToFile() {
        String contactListString = contactList.stream().map(cnt -> cnt.toString()).collect(Collectors.joining("\r\n"));
        try (OutputStream os = new FileOutputStream("contactList.txt")) {
            os.write(contactListString.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //read contacts from file to ArrayList contactList
    private void readContactListFromFile() {
        contactList.clear();
        //read contactList from file
        try (InputStream is = new FileInputStream("contactList.txt")) {
            Scanner scanner = new Scanner(is);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.trim().isEmpty()) continue;
                String[] parts = line.split(":");
                String surname = parts[0];
                String name = parts[1];
                String patronomic = parts[2];
                TypeContact type = Objects.equals(parts[3], "EMAIL") ? TypeContact.EMAIL : TypeContact.PHONE;
                String value = parts[4];
                String uuid = parts[5];
                contactList.add(new Contact(surname, name, patronomic, type, value, uuid));

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void deleteContact(String value) {
        List<Contact> cList = contactList.stream().filter(o -> o.getValue().equals(value)).collect(Collectors.toList());
        for (Contact cnt : cList
        ) {
            //  System.out.println("удаляется контакт: "+cnt);
            contactList.remove(cnt);
        }
        writeContactListToFile();
    }

    @Override
    public List<Contact> searchByName(String namePart) {
        return contactList.stream().filter(o -> (o.getSurname() + " " + o.getName() + " " + o.getPatronomic()).indexOf(namePart) != -1).collect(Collectors.toList());
    }

    @Override
    public List<Contact> SearchByStartOfContact(String valueStart) {
        return contactList.stream().filter(o -> o.getValue().startsWith(valueStart)).collect(Collectors.toList());

    }

    @Override
    public List<Contact> SelectContactsWithSorting() {

        return contactList.stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> SelectByType(TypeContact type) {
        return contactList.stream()
                .filter(o -> o.getType() == type)
                .collect(Collectors.toList());

    }

    @Override
    public Optional<Contact> SearchByValue(String value) {
        return contactList.stream()
                .filter(o -> o.getValue()
                        .equals(value))
                .findFirst();
    }
}
*/
