package services;

import db.BeanPropertyRowMapper;
import db.JdbcTemplate;
import db.Md5PasswordEncoder;
import dto.UserDbDto;
import entities.User;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
public class DbaseAuthService implements AuthService {
    private final JdbcTemplate jdbcTemplate;
    private final Md5PasswordEncoder passwordEncoder;

    @Getter
    private boolean auth;
    List<User> users = new ArrayList<>();

    /*public DbaseAuthService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }*/


    @Override
    public void login(String login, String pass) {
       // ResultSet resultSet =  jdbcTemplate.querySimple("select login from users where login='"+login+"' AND password='"+pass+"'");
       List<UserDbDto> userList = jdbcTemplate.query("select login from users where login='"+login+"' AND password='"+passwordEncoder.encode(pass)+"'",new BeanPropertyRowMapper<>(UserDbDto.class));
       if (userList.size()>0){
           auth = true;
           System.out.println("Аутентификация в репозитории успешна!");
       } else {
           System.out.println("Неверный логин или пароль!");
       }

       /* try {
            while (resultSet.next()) {
                auth = true;

            }
            System.out.println("Аутентификация в репозитории успешна!");
        } catch (SQLException throwables) {
            System.out.println("Неверный логин или пароль!");
            throwables.printStackTrace();

        }*/
    }

    @Override
    public void register(String login, String dataBorn, String pass) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parsedDate = null;
        try {
            parsedDate = dateFormat.parse(dataBorn);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
        jdbcTemplate.execute("insert into users ( login, password, date_born) values\n" +
                "('"+login+"','"+passwordEncoder.encode(pass)+"','"+timestamp+"')");

    }

    @Override
    public boolean isAuth() {
        return auth;
    }
}
