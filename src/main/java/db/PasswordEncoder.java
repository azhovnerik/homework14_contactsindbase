package db;

public interface PasswordEncoder {
    public String encode(String password) ;
    public boolean verify(String password,String hash) ;

}
