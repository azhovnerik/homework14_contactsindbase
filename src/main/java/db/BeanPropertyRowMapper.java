package db;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.function.Function;

@RequiredArgsConstructor
public class BeanPropertyRowMapper<T> implements Function<ResultSet,T> {
   private final Class<T> clazz;
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    public T apply(ResultSet resultSet) {
        Object value;
        Field[] fields = clazz.getDeclaredFields();
        try {
            T instance = clazz.getConstructor().newInstance();
            for (Field field: fields
                 ) {
                String name = field.getName(); //TODO to snake case

                if (resultSet.getObject(name) instanceof Timestamp) {
                    Object valueDb = resultSet.getObject(name);
                    value = format.format(valueDb);
                } else {
                    value = resultSet.getObject(name, field.getType());
                }
                field.setAccessible(true);
                field.set(instance,value);
            }
            return instance;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
