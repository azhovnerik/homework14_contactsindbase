package db;

import lombok.AllArgsConstructor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@AllArgsConstructor
public class JdbcTemplate {
    private DataSource dataSource;
    public <T> List<T> query(String query, Function<ResultSet,T> converter){
        try( Connection connection = dataSource.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            List<T>  list = new ArrayList<>();
            while (resultSet.next()){
                list.add(converter.apply(resultSet));

            }
            return list;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    //метод получает выборку и возвращает ее без всяких конвертаций
    public ResultSet querySimple(String query){
        try( Connection connection = dataSource.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            return resultSet;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    //метод модифицирует данные в БД
    public boolean execute(String query){
        try( Connection connection = dataSource.getConnection()){
            Statement statement = connection.createStatement();
            Boolean result = statement.execute(query);
            return result;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
