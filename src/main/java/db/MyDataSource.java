package db;

import lombok.AllArgsConstructor;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

@AllArgsConstructor
public class MyDataSource implements DataSource {
    private String url;
    private String user;
    private String password;

    @Override
    public Connection getConnection() throws SQLException {
        return  DriverManager.getConnection("jdbc:postgresql://localhost:5432/DBcontacts",
                "postgres",
                "deportivo");
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return  DriverManager.getConnection(url,
                username,
                password);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
