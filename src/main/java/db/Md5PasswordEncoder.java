package db;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5PasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(password.getBytes());
        byte[] digest = md.digest();
        StringBuilder builder = new StringBuilder();
        for  (byte b : digest) {
           builder.append(String.format("%x",b));
        }
         return builder.toString();
    }

    @Override
    public boolean verify(String password, String hash) {
        return hash.equals(encode(password));
    }
}
