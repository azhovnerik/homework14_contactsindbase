import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import db.JdbcTemplate;
import db.Md5PasswordEncoder;
import db.MyDataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import persistence.ContactRepository;
import persistence.DbaseContactRepository;
import services.DbaseAuthService;

import javax.sql.DataSource;
import java.util.Properties;

@RequiredArgsConstructor
@Slf4j
public class DbaseContactRepositoryFactory implements ContactRepositoryFactory {
    private final Properties properties;
    private JdbcTemplate jdbcTemplate;
    private Md5PasswordEncoder passwordEncoder;
    @Override
    public ContactRepository createContactRepositort() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/DBcontacts");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("deportivo");
        hikariConfig.setMaximumPoolSize(10);
        hikariConfig.setMinimumIdle(5);

        DataSource dataSource = new HikariDataSource(hikariConfig);
        jdbcTemplate = new JdbcTemplate(dataSource);
        passwordEncoder = new Md5PasswordEncoder();
        DbaseAuthService as  = createAuthService();
       // DataSource dataSource = new MyDataSource("jdbc:postgresql://localhost:5432/DBcontacts","postgres", "deportivo");

        return new DbaseContactRepository(as,jdbcTemplate);
    }
    private DbaseAuthService dbaseAuthService;

    @Override
    public DbaseAuthService createAuthService() {
        if (dbaseAuthService==null){
            dbaseAuthService = new DbaseAuthService(jdbcTemplate,passwordEncoder);
        }
        return dbaseAuthService;
    }
}
