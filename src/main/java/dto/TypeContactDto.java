package dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TypeContactDto {

    @JsonProperty("email")
    EMAIL("email:"),
    @JsonProperty("phone")
    PHONE("phone:");
    private String name;

    TypeContactDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
