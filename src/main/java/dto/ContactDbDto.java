package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import entities.Contact;
import entities.TypeContact;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Data
@Setter
@RequiredArgsConstructor
public class ContactDbDto extends Contact {
    private Integer id;
    private String name;
    private Integer type_Id;
    private String value;
   // private String fullName;

    public ContactDbDto(int id,String name, int typeId, String value) {
        this.id = id;
        this.name = name;
        this.type_Id =  typeId;  //getTypeFromTypeId(typeId);
        this.value = value;

    }




    @Override
    public String toString() {
        return  name+":"+ TypeContact.getTypeFromTypeId(type_Id)+value+":";
    }


}
